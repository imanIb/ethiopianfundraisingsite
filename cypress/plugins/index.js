/// <reference types="cypress" />

const faker = require('faker');

/**
 * @type {Cypress.PluginConfig}
 */

module.exports = (on, config) => {
  on("task", {
    fetchFakeUser() {
      user = 
        {
          "fullName": faker.name.firstName(),
          "email": faker.internet.email(),
          "password": faker.internet.password(),
          "phoneNo": faker.phone.phoneNumber(),
        };
      return user;
    },
    // fetchFakeFundRequest() {
    //   fundRequest = 
    //     {
    //       firstName: "",
    //       lastName: "",
    //       age: ,
    //       gender,
    //       recoveryCost,
    //       maritalStatus,
    //       description,
    //       diagnosis,
    //       verificationFile,
    //       patientId,
    //       kebeleIdFile,
    //       medicalFile
    //     };
    //   return fundRequest;
    // }
  })
  return config
}
