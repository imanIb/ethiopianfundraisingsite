/// <reference types="cypress" />

let fakeUser;

describe('Given the Users api', () => {
  beforeEach(() => {
    cy.task('fetchFakeUser').then((user) => {
      fakeUser = user;
      fakeUser.role = "admin";
    });
  });

  context('When I send POST /fund-requests', () => {
    it('Then it should create a new user', () => {
      cy.request({
        method: 'POST',
        url: '/fund-requests',
        body: fakeUser
      })
        .should((response) => {
          expect(response.status).eq(200)
          cy.log(JSON.stringify(response.Status))
        });
    });
  });

});