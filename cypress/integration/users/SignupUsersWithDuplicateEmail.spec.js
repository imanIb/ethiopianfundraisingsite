/// <reference types="cypress" />

let fakeUser;

describe('Given the Users api', () => {
  beforeEach(() => {
    cy.task('fetchFakeUser').then((user) => {
      fakeUser = user;
      fakeUser.role = "admin";
      cy.request({
        method: 'POST',
        url: '/users/signup',
        body: fakeUser
      });
    });
  });

  context('When I send POST /users/signup with duplicate email', () => {
    it('Then it should return Duplicate Email Error', () => {
      cy.request({
        method: 'POST',
        url: '/users/signup',
        body: fakeUser,
        failOnStatusCode: false
      })
        .should((response) => {
         expect(response.status).eq(500)
        });
    });
  });
});