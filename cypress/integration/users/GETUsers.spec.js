/// <reference types="cypress" />

describe('Given the Users api', () => {
  context('When I send GET /users', () => {
    it('Then it should return a list with all registered users', () => {
      cy.request({
        method: 'GET',
        url: '/users'
      })
        .should((response) => {
          expect(response.status).to.eq(200)
          Cypress._.each(response.body, (user) => {
            cy.log(JSON.stringify(user))
            expect(user.email).to.not.be.null
           expect(user).to.have.any.keys('_id', 'fullName', 'email', 'phoneNo', 'role')
          })
        });
    });
  });

  context('When I send GET /users passing role as param', () => {
    it('Then it should return only the filtered user', () => {
      cy.request({
        method: 'GET',
        url: '/users/patient',
      })
        .should((response) => {
          expect(response.status).to.eq(200)
          Cypress._.each(response.body, (user) => {
            cy.log(JSON.stringify(user))
            expect(user.role).to.eq("patient")
          })
        });
    });
  });
});