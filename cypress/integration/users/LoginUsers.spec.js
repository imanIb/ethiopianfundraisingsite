/// <reference types="cypress" />

let fakeUser;

describe('Given the Users api', () => {
  beforeEach(() => {
    cy.task('fetchFakeUser').then((user) => {
      fakeUser = user;
      fakeUser.role = "admin";
      cy.request({
        method: 'POST',
        url: '/users/signup',
        body: fakeUser
      });
    });
  });

  context('When I send POST /users/login with valid credential', () => {
    it('Then it should return 200', () => {
      cy.request({
        method: 'POST',
        url: '/users/login',
        body: { email: fakeUser.email, password: fakeUser.password}
      })
        .should((response) => {
          expect(response.status).eq(200)
        });
    });
  });

  context('When I send POST /users/login with invalid credential', () => {
    it('Then it should return 401', () => {
      cy.request({
        method: 'POST',
        url: '/users/login',
        body: { email: fakeUser.email, password: "1234"},
        failOnStatusCode: false
      })
        .should((response) => {
          expect(response.status).eq(401)
        });
    });
  });

});