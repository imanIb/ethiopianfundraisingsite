var User = require('../models/User');
const bcrypt = require('bcrypt');

exports.findAllUsers = function (returnFn){
    User.find({}, function(err, res){
        if(err)
            return returnFn(err);
        returnFn(res);
    });
}

exports.findUsersByRole = function (role, returnFn){
    User.find({role: role}, function(err, res){
        if(err)
            return returnFn(err);
        returnFn(res);
    })
}

exports.insertUser = function (fullName, email, phoneNo, password, role, returnFn){
    let user = new User({fullName: fullName, email: email, phoneNo: phoneNo,password: password, role: role});
    user.save((err, res)=>{
        if(err){
        if(err.keyPattern.email){
            return returnFn({error: true, message: "Email Duplicate"});
        }

        return returnFn({error: true, message: "Error"});
        }
        returnFn(res);
    });
    
}
exports.findByUsername = function(username, returnFn){
    User.find({fullName: username},function(err, res){
        if(err)
            return returnFn(err);
        returnFn(res);
    });

}

exports.updateAUser = function(id, newUser, returnFn){
    User.findByIdAndUpdate({_id: id}, newUser,{new: true}, function(err, result){
        if(err) 
            return returnFn(err); 
        returnFn(result); 
    });

}

exports.loginUser = function(email, password, returnFn){
    User.findOne({email: email}, function(err, res){
        if(!res) return returnFn({error: true, message: "Invalid Credential"}); 
        
        if(err){
            return returnFn({error: true, message: err});
        }
        else{
            let profile = {
                id: res._id,
                fullName: res.fullName,
                email: res.email,
                role: res.role,
                status: res.status    
            }
            bcrypt.compare(password, res.password, function(err, isValid){
                if(err || !isValid){
                   return returnFn({error: true, message: "Invalid Credential"}); 
                }else {
                   
                    returnFn({error: false, message: 'Login Success', profile: profile});
                }
            })
        }
    })
}

exports.blockUser = function(userId, returnFn){
    User.findByIdAndUpdate({_id: userId}, {status: 'blocked'}, {new: true}, function(err, result){
        if(err)
            return returnFn(err);
        returnFn(result);
    })
}

exports.findUserById = function(id, returnFn){
    User.findById(id, function(err, result){
        if(err)
            return returnFn({error: err});
        returnFn({success: 'User found'});
    })
}