require('dotenv').config();

const mongoose = require('mongoose');
var dbUrl = encodeURI(process.env.DB_URL);
//'mongodb://localhost/tena_db';
//'mongodb+srv://tena:n4LqmYzQuHoLfg4y@hellotaxi.f3gyr.gcp.mongodb.net/tena_db?retryWrites=true&w=majority';
//'mongodb://localhost/tena_db';
//'mongodb+srv://tena:n4LqmYzQuHoLfg4y@hellotaxi.f3gyr.gcp.mongodb.net/tena_db?retryWrites=true&w=majority';
//'mongodb://localhost/Tena'
mongoose.connect(dbUrl,{useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;

db.on('err', () => console.error.bind(console, 'Connection Error!'));
db.once('open', () => console.log('Connected to database'));

module.exports = db;