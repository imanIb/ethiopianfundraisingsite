var express = require('express');
var router = express.Router();

var RequestController = require('../controllers/RequestController');

router.post('/',  RequestController.createFundRequest);

router.get('/',  RequestController.getAllFundRequests);

router.get('/:id',  RequestController.getFundRequestById);

router.get('/status/:status',  RequestController.getFundRequestsByStatus);

router.put('/updateStatus/:status/:id',  RequestController.updateStatus);

module.exports = router;
