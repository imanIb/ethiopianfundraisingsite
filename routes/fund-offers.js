var express = require('express');
var router = express.Router();

var FundOfferController = require('../controllers/FundOfferController');

router.post('/',  FundOfferController.offerFund);

module.exports = router;
