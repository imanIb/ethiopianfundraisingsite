var express = require('express');
var router = express.Router();
const BankController = require('../controllers/BankController');

router.post('/debit', BankController.debit);

module.exports = router;