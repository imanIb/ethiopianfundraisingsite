const { uuid } = require('uuidv4');
var firebaseAdmin = require("firebase-admin");

class UploadFile {
    async execute(folder, file) {
        try {
            let storage = firebaseAdmin.storage();

            let token = uuid();
            let filename = file.name;

            const bucket = storage.bucket();
            const imageBuffer = new Uint8Array(file.data);
            const bucketFile = bucket.file(`${folder}/${filename}`, {
                uploadType: {resumable: false}
            });

            await bucketFile.save(imageBuffer, async (err) => {
                if (err) {
                    console.error(`Error uploading: ${filename} with message: ${err.message}`);
                    return;
                }
            });

            await bucketFile.setMetadata({
                metadata: {
                  firebaseStorageDownloadTokens: token
                },
              })

              let storageBucket = 'tena-56251.appspot.com';

        let url = this.generatePublicUrl(storageBucket, folder, filename, token);

        return url;

        } catch (error) {
            console.log(error);
        }
    }

    generatePublicUrl(bucket, folder, filename, token) {
        return `https://firebasestorage.googleapis.com/v0/b/${bucket}/o/${folder}%2F${filename}?alt=media&token=${token}`;
    }
}

module.exports = new UploadFile();
