var RequestService = require("../services/RequestService");

exports.createFundRequest = function (req, res, next) {
 // return res.status(res.statusCode).json(req.files);
  RequestService.createRequest(
    req,
    (result) => {
      res.status(res.statusCode).json(result);
    }
  );
};

exports.getAllFundRequests = function (req, res, next) {
  RequestService.getAllRequests((result) => {
    res.status(res.statusCode).json(result);
  });
};

exports.getFundRequestById = function (req, res, next) {
  let { id } = req.params;

  RequestService.getRequestById(id, (result) => {
    res.status(res.statusCode).json(result);
  });
};

exports.getFundRequestsByStatus = function (req, res, next) {
  let { status } = req.params;
  RequestService.getRequestsByStatus(status, (result) => {
    res.status(res.statusCode).json(result);
  });
};

exports.updateStatus = function (req, res, next) {
  let { id, status } = req.params;
  RequestService.updateStatus(id, status, (result) => {
    res.status(res.statusCode).json(result);
  });
};
