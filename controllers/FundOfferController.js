var OfferService = require('../services/OfferServices');

exports.offerFund = function(req, res, next){
    let {
        bankType,
        amount,
        accountNo,
        requestId,
        providerId
    } = req.body;

    OfferService.offerFund(
        bankType,
        amount,
        accountNo,
        requestId,
        providerId
        ,(result)=>{
        res.status(res.statusCode).json(result);
    });
}