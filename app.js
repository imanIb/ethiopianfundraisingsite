var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
const fileUpload = require('express-fileupload');
require('./core/firebase-init');

var usersRouter = require('./routes/users');
var banksRouter = require('./routes/banks');
var fundRequestRouter = require('./routes/fund-requests');
var fundOfferRouter = require('./routes/fund-offers');

var app = express();
var db = require('./config/connectDb');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());

app.use('/users', usersRouter);
app.use('/banks', banksRouter);
app.use('/fund-requests', fundRequestRouter);
app.use('/fund-offers', fundOfferRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
