const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let requestSchema = new Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    age: {type: String, required: true},
    gender : {type: String, required: true},
    maritalStatus: {type: String, required: true},
    description: {type: String, required: true},
    diagnosis: {type: String, required: true},
    kebeleIdFileUrl: {type: String, required: false},
   // verificationFile: {data: Buffer, contentType: String},
    medicalFileUrl: {type: String, required: false},
    recoveryCost: {type: Number, required: true},//does recoveryCost same as cost?
    status: {type: String, default: 'accepted'},
    progress: {type: Number, default: 0},
    rateAmount: {type: Number, default: 0},
    patientId: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    requestedAt: {type: Date, default: Date.now},
});

module.exports = mongoose.model('FundRequest', requestSchema);